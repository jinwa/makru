# Simple Sample

This sample contains a simple configuration and a sample plugin.

Try these:
````
makru -h
makru :list_actions
makru
makru :sayhello
makru :sayhello --addtional-message="Hallo" --addtional-message="Guten Morgen"
````
Look at `makru/plugins/plugin-simple.py` to exprole these features.
