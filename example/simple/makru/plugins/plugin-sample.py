from makru import Sandbox, Makru, OptionDescription

# Hook compile
# this hook will be called if this plugin defined "use" in config.
# The first argument is an instance of CompilingDesc
def compile(desc):
    print("Plugin Sample!")
    print("name: {}".format(desc.name))
    print("type: {}".format(desc.type))
    print("root path: {}".format(desc.root))
    print("==== Config ====")
    print(desc.config)
    print("========")
    print("==== Dependencies ====")
    print(desc.dependencies)
    print("========")
    print("==== Global Variables ====")
    print(desc.gvars)
    print("========")


def get_template(lang):
    if lang == "pinyin":
        return "ni hao, {}!"
    else:
        return "hello, {}!"


def say_hello_world(desc):
    lang = desc.gvars.get("language", "english").lower()
    template = get_template(lang)
    if desc.action_arg:
        print(template.format(desc.action_arg))
    else:
        print(template.format("world"))
    for msg in desc.gvars["addtional-message"]:
        print(msg)


def on_sandbox_load(sandbox: Sandbox):
    print("!!Sandbox loading!!")

    @sandbox.add_private_action("_sample_private_action")
    def _sample_private_action(desc):
        print("private action (normally it should not be called by users)")


def after_configuration_load(makru: Makru):
    makru.definde_option(
        "language",
        OptionDescription(
            plugin_name="plugin-sample",
            description="select the language you want, chioces: english, pinyin",
            multiple=False,
            bool_value=False,
        ),
    )
    makru.definde_option(
        "addtional-message",
        OptionDescription(
            plugin_name="plugin-sample",
            multiple=True,
            description="addtional hallo message",
        ),
    )
    makru.definde_option(
        "LANG", OptionDescription(plugin_name="plugin-sample", mix_into="language")
    )
