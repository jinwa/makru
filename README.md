# Makru
[![pipeline status](https://gitlab.com/jinwa/makru/badges/master/pipeline.svg)](https://gitlab.com/jinwa/makru/-/commits/master)
[![coverage report](https://gitlab.com/jinwa/makru/badges/master/coverage.svg)](https://gitlab.com/jinwa/makru/-/commits/master)  
Makru(/ˈmeɪkru/) is a project management framework.

## Installation
Installing through PyPI is recommanded.
````
pip install makru
````

## Documents
- [Quick Start with Makru](docs/quick_start.md)
- [Plugins for Makru](docs/plugins.md)
- [Write Plugin Youself](docs/wrt_plugin.md)
- [CLI Reference](docs/cli.md)
- [Contribution Guide](CONTRIBUTION.md)

## Related Projects
- [marku_langc](https://gitlab.com/jinwa/makru_langc) is a plugin for C project.

## License
The MIT License. See `LICENSE` for details.
This project is owned by all contributors, see `CONTRIBUTORS` for full list.
