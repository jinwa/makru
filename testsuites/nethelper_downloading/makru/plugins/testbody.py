import subprocess
from makru.nethelpers import download
from makru.helpers import exprog
from makru import CompilingDesc, panic
from pathlib import Path
import sys


def compile(desc: CompilingDesc):
    tmp_path = Path(desc.gvars["tmp-path"])
    test_file = tmp_path / "testfile.txt"
    download("http://localhost:61203/", str(test_file), show_progress=True)
    with open(str(test_file)) as f:
        assert len(f.read()) == 8192
