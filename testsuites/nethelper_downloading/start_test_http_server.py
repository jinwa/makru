from http.server import SimpleHTTPRequestHandler, HTTPServer
from random import Random
import sys

RAND = Random()

BASESTRS = "abcdefghijklnmopqrstuvwxyz0123456789"


def generate_random_byte_string(length: int):
    return bytes("".join(RAND.choices(BASESTRS, k=length)), "utf-8")


TESTDATA = generate_random_byte_string(8192)


class TestHTTPServerHandler(SimpleHTTPRequestHandler):
    def do_GET(self) -> None:
        self.send_response(200)
        self.send_header("Content-Length", "8192")
        self.end_headers()
        self.wfile.write(TESTDATA)


def create_test_http_server():
    server = HTTPServer(("127.0.0.1", 61203), TestHTTPServerHandler)
    return server


print("starting server on 127.0.0.1:61203")
server = create_test_http_server()
server.serve_forever()
