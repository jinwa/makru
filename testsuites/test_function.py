from makru import Makru, panic
from pathlib import Path
from makru.helpers import create_makru, exprog
import sys
import time


def get_testsuite_path(name):
    return Path(__file__).with_name(name)


def create_testsuite(name, *extra_args):
    return create_makru("-F{}".format(get_testsuite_path(name)), *extra_args)


class TestFunction:
    def test_plugin_cross_importing(self, capsys):
        ins = create_testsuite("plugin_cross_importing")
        ins.main()
        captured = capsys.readouterr()
        assert captured.out == "Hello, World!\n"

    def test_nethelper_download(self, tmp_path):
        subp = exprog(
            (sys.executable, "start_test_http_server.py"),
            cwd=get_testsuite_path("nethelper_downloading"),
        )
        ins: Makru = create_testsuite(
            "nethelper_downloading", "--tmp-path={}".format(tmp_path)
        )
        try:
            time.sleep(1)
            ins.main()
        finally:
            subp.kill()
            subp.wait()
            sys.stdout.write(subp.stdout.read())
            sys.stderr.write(subp.stderr.read())
            if subp.returncode != 0 and subp.returncode != -9:
                panic("failed with {}".format(subp.returncode))

    def test_nethelper_check_network(self):
        TS_NAME = "nethelper_check_network"
        subp = exprog(
            (sys.executable, "start_test_http_server.py"),
            cwd=get_testsuite_path(TS_NAME),
        )
        ins: Makru = create_testsuite(TS_NAME)
        try:
            time.sleep(1)
            ins.main()
        finally:
            subp.kill()
            subp.wait()
            sys.stdout.write(subp.stdout.read())
            sys.stderr.write(subp.stderr.read())
            if subp.returncode != 0 and subp.returncode != -9:
                panic("failed with {}".format(subp.returncode))

    def test_async_action(self, capsys):
        ins: Makru = create_testsuite("async_action")
        ins.main()
        captured = capsys.readouterr()
        assert captured.out == "Hello, Async Actions!\n"

    def test_async_hook(self, capsys):
        ins: Makru = create_testsuite("async_hook")
        ins.main()
        captured = capsys.readouterr()
        assert "Hook1" in captured.out
        assert "Hook2" in captured.out
