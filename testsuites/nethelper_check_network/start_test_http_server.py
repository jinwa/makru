from http.server import SimpleHTTPRequestHandler, HTTPServer


class TestHTTPServerHandler(SimpleHTTPRequestHandler):
    def do_GET(self) -> None:
        if self.path != "/hold":
            self.send_response_only(204)
            self.end_headers()
        else:
            while True:
                pass


def create_test_http_server():
    server = HTTPServer(("127.0.0.1", 61203), TestHTTPServerHandler)
    return server


print("starting server on 127.0.0.1:61203")
server = create_test_http_server()
server.serve_forever()
