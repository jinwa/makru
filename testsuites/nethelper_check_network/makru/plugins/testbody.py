from makru.nethelpers import check_network
from makru import CompilingDesc


def compile(desc: CompilingDesc):
    result = check_network("http://127.0.0.1:61203/")
    assert result == True
    result2 = check_network("http://127.0.0.1:61203/hold", timeout=1)
    assert result2 == False
