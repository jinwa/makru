# Contribution Guide

1. Fork. 
2. Work on your copy. (I recommend you to work on the `develop` branch, but `master` is okay)
3. Make a pull request to `develop` branch in this repo.

## Difference between `master` and `develop`
- `master` is the public default branch saves stable version of the code.
- `develop` is a private branch own by maintainer.

## Hello, poetry!
Makru is using [poetry](https://python-poetry.org) now.

## Code Style
Just use [black](https://github.com/psf/black) to format code. You can use `makru :fmt` as the shortcut.

## Releases
The code in `master` branch is stable version. When releaseing one version, the bumpping commit will be tagged and a new `Release` will be published on GitLab (sometimes later). This project uses [Semantic Versioning 2.0.0](https://semver.org/). The bump of "patch" does not lead to new alpha version, the "minor" or "major" does.

- Major Updates: major+1 -> alpha versions -> beta versions -> rc versions -> release
- Minor Updates: minor+1 -> alpha versions -> beta versions -> rc versions -> release
- Patch Updates: patch+1

For example, in a major update flows:
1.2.3 -> 2.0.0-alpha.0 -> 2.0.0-alpha.1 -> ... -> 2.0.0-beta.0 -> 2.0.0-beta.1 -> ... -> 2.0.0-rc.0 -> 2.0.0-rc.1 -> 2.0.0

But in patch update:
1.2.3 -> 1.2.4

### Quick Bumping
Use `poetry version` to bump versions.

## Directory Layout
- `makru` - main source direcotry
- `example` - saves samples
- `docs` - document directory
- `testsuites` - saves functional tests
- `tools` - saves tools and makru plugins for this project
- `.gitlab-ci.yml` - GitLab CI configuration
- `CONTRIBUTORS` - names of all contributors

## Tests
| Branch  | Coverage                                                                                                                           |
|---------|------------------------------------------------------------------------------------------------------------------------------------|
| master  | [![coverage report](https://gitlab.com/jinwa/makru/badges/master/coverage.svg)](https://gitlab.com/jinwa/makru/-/commits/master)   |
| develop | [![coverage report](https://gitlab.com/jinwa/makru/badges/develop/coverage.svg)](https://gitlab.com/jinwa/makru/-/commits/develop) |


Every part of this project shall have correct tests. This project uses [pytest](https://pytest.org) as test framework. Small tests are placed into `makru`, the source directory, with prefix name `test_`. Tests which need a complete environment to run should be placed into `testsuites` and add the test code to `testsuites/test_function.py`.

Use `makru :test` and `makru :test-cov`(with coverage) as shortcuts.


## Packing and Uploading
Use `poetry publish`.

## Git hooks
### Installing
Copy or link file(s) under "docs/dev_helpers/git_hooks" to ".git/hooks".

````sh
cp -Rl docs/dev_helpers/git_hooks/* .git/hooks/
````

### Options
#### `hooks.allownonascii`
Allow non-ASCII character.

````sh
git config hooks.allownonascii true
````

#### `hooks.autoformatcode`
Auto format code before commiting.

````sh
git config hooks.autoformatcode true
````
