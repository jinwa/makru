from pathlib import Path
from makru import CompilingDesc, panic
import semver
from makru.helpers import shell


def version_remove_part(version: semver.VersionInfo, part: str, value=None):
    version_info_d = version.to_dict()
    version_info_d[part] = value
    return semver.VersionInfo(**version_info_d)


def non_zero_to_fail(code: int):
    if code != 0:
        panic("failed: exited with {}".format(code))


def bump(desc: CompilingDesc):
    version_file_path = Path(desc.root) / "VERSION"
    method = desc.action_arg
    version_info = None
    with open(version_file_path) as f:
        version_info = semver.VersionInfo.parse(f.read())
    assert version_info
    if not method:
        panic(
            "bump: the arguments should be one of alpha, beta, rc, patch, minor, major, release"
        )
    version_info = version_remove_part(version_info, "build")
    if method == "alpha":
        if version_info.prerelease and version_info.prerelease.startswith("alpha"):
            version_info = version_info.bump_prerelease("alpha")
        else:
            panic(
                "only allow bump alpha while alpha, bump major or minor if you want a alpha version."
            )
    elif method == "beta":
        if version_info.prerelease and version_info.prerelease.startswith("alpha"):
            version_info = version_info.replace(prerelease="beta.0")
        elif version_info.prerelease.startswith("beta"):
            version_info = version_info.bump_prerelease("beta")
        else:
            panic("only allow bump beta from alpha")
    elif method == "rc":
        if version_info.prerelease and version_info.prerelease.startswith("beta"):
            version_info = version_info.replace(prerelease="rc.0")
        elif version_info.prerelease.startswith("rc"):
            version_info = version_info.bump_prerelease("rc")
        else:
            panic("only allow bump rc from beta")
    elif method == "patch":
        version_info = version_info.bump_patch()
    elif method == "minor":
        version_info = version_info.replace(patch=0, prerelease="alpha.0")
        version_info = version_info.bump_minor()
    elif method == "major":
        version_info = version_info.replace(patch=0, minor=0, prerelease="alpha.0")
        version_info = version_info.bump_major()
    elif method == "release":
        version_info = version_remove_part(version_info, "prerelease")
    print("bump to v{}".format(version_info))
    if not desc.gvars.get("dryrun", False):
        with open(version_file_path, mode="w+") as f:
            f.write(str(version_info))
    return version_info


def bump_and_tag(desc: CompilingDesc):
    version_info: semver.VersionInfo = bump(desc)
    if not desc.gvars.get("dryrun", False):
        non_zero_to_fail(shell(["git", "add", "VERSION"], cwd=desc.root))
        non_zero_to_fail(
            shell(
                ["git", "commit", "-mbump to v{}".format(version_info)], cwd=desc.root
            )
        )
        non_zero_to_fail(
            shell(["git", "tag", "v{}".format(version_info)], cwd=desc.root)
        )


def release_push(desc: CompilingDesc):
    if not desc.gvars.get("dryrun", False):
        non_zero_to_fail(shell(["git", "push"], cwd=desc.root))
        non_zero_to_fail(shell(["git", "checkout", "master"], cwd=desc.root))
        non_zero_to_fail(shell(["git", "merge", "develop"], cwd=desc.root))
        non_zero_to_fail(shell(["git", "push"], cwd=desc.root))
        non_zero_to_fail(shell(["git", "push", "--tags"], cwd=desc.root))
        non_zero_to_fail(shell(["git", "checkout", "develop"], cwd=desc.root))
