import os


def shell(desc):
    parts = desc.action_default_arg.split(" ")
    path = parts[0]
    args = parts
    os.execvp(path, args)
