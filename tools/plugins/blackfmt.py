from makru.helpers import shell
from pathlib import Path
from makru import CompilingDesc


def runblack(desc: CompilingDesc):
    root = Path(desc.root)
    shell(["black", str(root / "testsuites")])
    shell(["black", str(root / "makru")])
    shell(["black", str(root / "tools" / "plugins")])
    shell(["black", str(root / "example")])
