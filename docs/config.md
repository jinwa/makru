# Makru Configuration
Makru uses a configuration file to understand what you need while running actions or doing compiling.
Makru will automatically read the `makru.yaml` under the current working folder, but you can still tell makru use another file or target another folder as current working folder by `-F <path_of_folder_or_file>`.

## Basic Structure
Makru uses YAML format as the configuration format.
In your configuration, it must contain three fields: `name`, `use` and `type`:
````yaml
name: sample
type: sample
use: someone_plugin
````
Makru uses the value of `use` to identify plugin to use compiling program (default behaviour in CLI, `-C` and action `compile`). Generally `name` and `type` may use by `plugin` to identify the policy while running.

## Actions
To use the actions defined in plugins, you must declare them in your configuration.
````yaml
actions:
  rin: piphelper:reinstall
````
### Normal Action Declaring
Directly set a key-value pair in `actions` to declaring a normal action.
````yaml
actions:
  rin: piphelper:reinstall
````
The key is the name of the action, which used in makru CLI; the value is writing in a format `<plugin>:<function_name>`, makru use it to find out which function to call.

### Declaring Action with Default Argument
Makru allows user pass a argument to one action in CLI, but in some cases it will be better if we can change the function behaviour in action declaring.
````yaml
actions:
  test-cov: shell:shell(pytest --cov=makru)
````
To declaring action with default argument, write the value as format `<plugin>:<function_name>(<default_argument>)`. The action function can recvive it by `CompilingDesc.action_default_arg`.

## Customise Plugins' Search Paths
By default makru search the `./makru/plugins` in the folder include configuration file, but in some cases you need to customise the search paths of plguins.  
To add search paths, use field `plugin_paths`.
````yaml
plugin_paths:
  - ./tools/plugins
````
In this case, `.` will be expanded to the folder the configuration file in.

