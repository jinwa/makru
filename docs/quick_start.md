# Quick Start with Makru

> TL;DR: Makru is very easy to start with. Place plugins and write your configuration. 

Makru itself does not contain any logic about compilation anything, it just collect infomation from configuration and tell the correct plugin you want to do. As another point of view, makru is a shortcut to plugins.

Let's start with a simple folder:
````
- MyProject
  | - src
  | - makru.yaml
````

As you have seen, the folder called "MyProject" keeps two files: one is directory "src" and another is "makru.yaml".

Makru mainly works around the configuration, in this case it's `makru.yaml` which is default, to get infomation about how it go. But before we go into, we must choose a compiling plugin. It could be anything even one empty file (as makru's doing).

Compiling plugin is the plugin which we chose to compiling the project. It could be called "default plugin" because compiling is the default if you call makru without saying to call anything else. Which plugin you choose is depend on what kind of things to deal with. You can find a (non-complete) plugin list for makru [here](plguins.md).

But here, come on, it's just a forever empty project just for a show when anyone like you come here. So I will place a empty plugin for the project. By default, plugins are placed under `<your_project_root>/makru/plugins`. So I create a empty file there.
````
mkdir -p makru/plugins
touch makru/plugins/empty-plugin.py
````

We have move here:
````
- MyProject
  | - src
  | - makru
    | - plugins
      | - empty-plugin.py
  | - makru.yaml
````

Okay, that's time for configuration. Here is our `makru.yaml` for that project:
````yaml
name: MyProject # the project name, can be different to the folder name
use: empty-plugin # which plugin you want to use
````
It's a super simple configuration, but it will be more complex if you are using some actual plugins. Plugins may request you to provide infomation in this file.

Finally, run `makru` under root of the project. Then you will find a not-so-good message:
````
panic: plugin empty-plugin requested in /path/to/MyProject/makru.yaml does not support compile hook
````
Well, it's not so bad. This message will happen when the plugin is being detected by makru but it does not support a "hook". Hooks are plugin's functions exposed to makru. Our `empty-plugin` have nothing, so makru complains about our chosen does not know how to compile a program.

If you found something like not found, there might be something wrong:
````
panic: could not found plugin empty-plugin requested in /path/to/MyProject/makru.yaml, all plugins: [], searched paths: ["/path/to/MyProject/makru/plugins"]
````
If you actually found a bug in makru, please issue them here: https://gitlab.com/jinwa/makru/-/issues . Thanks your help!

Finally is here, that's about makru usage. Makru is very simple because of it's flexible design.

But that's not complete! Makru needs more words to cover. We won't let you down. Here are some resource you can take a look on (if avaliable):
- [CLI Reference](cli.md)
- Actions
- Configration Makru
- Deep Into How Makru Works
- [Makru Plugin List](plugins.md)
- [Write Plugin Yourself](./wrt_plugin.md)
