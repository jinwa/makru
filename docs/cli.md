# Makru CLI
````
makru [:<action> [<action_argument>] [:<action> [<action_argument>] [:<action> [<action_argument>] [...]]]] [-F<path>] [-C] [--<key>=<value> --<key> --!<key>...]

:<action> [<action_argument>] the <action> you want to call, optionally add <action_argument> as argument

Options:
    -F<path>        use <path> as makru config file or project folder needed to find makru config
    -C      start compiling in the end
    --<key>=<value>     define <key> as <value> in sandboxes
    --<key>     same as --<key>=true
    --!<key>    same as --<key>=false
````

## Examples
### Pipeline
````
makru :compile :install
````
`compile` is a built-in action, it has same affect as `makru -C`, but it can be chained in actions to make a pipeline. Any error will panic the program.
````
$ makru :compile :test :depoly
...
panic: the returncode does not 0: 1
````
More examples:
````
makru :fetchdeps -C
makru :signapk :release
makru :upver 0.1.0-beta.5 :push
````
### Action Arguments
It is possible to set a argument in action calling.
There are two ways:
````
makru :action(argument)
````
and
````
makru :action argument
````
Then the action can recvice the argument by `CompilingDesc.action_arg`.

Pipeline can also:
````
makru :test :placekey `$KEYFILE` :deploy production
````
