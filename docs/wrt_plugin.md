# Write Plugin Yourself

Makru accepts any general python module as a plugin. The plugin could be single file or directory (just like makru_langc doing).
Makru heavily depends on "hooks", which are callables exposed to makru. Your plugin mainly start running from them.
In conculusion, plugins are python modules + hook functions.

Let's start from here.

## A Simple Plugin
Now we create a new directory as root, and create a new file `simple.py`:
````python
def compile(desc):
    print("Hello World!")

````
It can be placed under root's `makru/plugins/`.
Then we write a configuration to use the plugin as compiling plugin:

````yaml
name: example_project
use: simple

````

Well done! As we run `makru` on the configuration, we will see:
````
Hello World!
````

Looks like everything is simple. But what's inside here?

When makru start running and detected that you does not ask for anything, it simpily sets the situation as the program runs with argument `-C`, which means that "user wants to compile". In this mode, makru will check if the compiling plugin have such a "hook" `compile`. If so, it will call this hook with `CompilingDesc` (which we will cover later), or leave something looks like:
````
panic: plugin empty-plugin requested in /path/to/MyProject/makru.yaml does not support compile hook
````

But just printing around does not make sence (well not, it makes you feel like that you are living in films (especially, "cracking"?)), and we want to take reals. Then you replace the `"Hello World"` to `Path(".").absolute()` just like:
````python
from pathlib import Path

def compile(desc):
    print(Path(".").absolute())

````
It looks working, but is not. This is a big failure: the path you got is current working directory. You can not depends on current working directory because it's could be different from to directory you need to work.

But how we can get correct infomation?

## `CompilingDesc`
You can see makru in fact provide one argument for your hook. It's a `CompilingDesc`, means CompilingDesc(ription). The instance keeps all infomation makru collected for you, and will be past to the hooks called by makru in most cases (even actions!), and is not limited to the `compile` hook.
````python
from makru import CompilingDesc

def compile(desc: CompilingDesc):
    pass

````
You can find the definition in [makru.py](https://gitlab.com/jinwa/makru/-/blob/master/makru/makru.py).
Let's back to the question: find out the root of the project. Just access `CompilingDesc.root`:
````python
from makru import CompilingDesc

def compile(desc: CompilingDesc):
    print("current root", desc.root)

````

That's super easy, right? If you want to find out a absolute path from a relative one (e.g. "./makru/langc"), you can use `CompilingDesc.expand_path` to avoid manual concating:
````python
def compile(desc):
    print("./makru/langc", desc.expand_path("./makru/langc"))
````

You can read the configuration by `CompilingDesc.config`:
````python
def compile(desc):
    print("config", desc.config)
````

And you can access makru's `Sandbox` instance by `CompilingDesc.sandbox`. By `Sandbox`, you can access the environment used by makru, such as plugins.

## Helpers
Makru provide a set of helpers to help you build plugins without third-party dependencies.
You can find them in https://gitlab.com/jinwa/makru/-/blob/master/makru/helpers.py . 

````python
from makru.helpers import check_binary

def compile(desc):
    print("compiler", check_binary('cc'))
````

## Store
Store is a key-pair database to store infomation about builds and project. Makru introduce two types of store: "build" and "info". "build" store is only for build and saved in middle-result directory and "info" store will be saved to project's makru private directory. Both of them should not be commit to VCS.

Stores could be access though `Sandbox`. But by default build store is not presented because of the unknown middle-result directory. It must be set by one plugin and it only can be set once.

Makru's implementation depends on sqlite3.

Let's make a small counter on the info store. It could be accessed by `Sandbox.info_store`, the store will not be create until accessing the field. The info store file is `/path/to/project/.makru/info.sqlite3`.

````python
from makru.store import Store, Table

def compile(desc):
    info_store: Store = desc.sandbox.info_store
    counter_table: Table = info_store.get_table("counter")
    COUNTER_KEY = "counter"
    if not counter_table.get(COUNTER_KEY):
        counter_table.set(COUNTER_KEY, "1")
        print("It's your first time call compile!")
    else:
        times = int(counter_table.get(COUNTER_KEY))
        print("counter: {}".format(times))
        counter_table.set(COUNTER_KEY, str(times+1))

````
As you see, one store can have multiple tables and tables is always eating strings for key and value. We should manually convert things between strings and types. The definition of `Store` and `Table` is here: https://gitlab.com/jinwa/makru/-/blob/master/makru/store.py .

What about build store? If you directly access build store, it just a `None`:

````python
def compile(desc):
    print("build_store", desc.sandbox.build_store)
````

````
$ makru
build_store None
````

We need to set the build by someone, you or a building framework, such as makru_langc. The build_store could not be set twice or more, or we will get panic.
We can set the build store though `Sandbox.set_build_store`.

````python
from makru.store import Store

def compile(desc):
    desc.sandbox.set_build_store(Store(":memory:"))
````

That the only one argument `Store` recvices is the path of a sqlite3 database, and it will be create if not exists. `:memory:` means it is in-memory.

But there could be a problem: the `Store` only create the non-exists file, but not the non-exists directory. There is a `Sandbox.make_store_under` to resolve the problem, the function will create the directories if it is non-exists.

````python
from makru import Sandbox

def compile(desc):
    desc.sandbox.set_build_store(Sandbox.make_store_under("/path/to", "build_store"))

````

The created file will be `/path/to/build_store.sqlite3`.
